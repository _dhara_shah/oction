# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository holds the demo for Oction
* Version 1.0

### How do I get set up? ###

* Use Android Studio the latest version, and the latest gradle tools
* Ensure that all the sdks are installed and also Android SDK is installed
* The following libraries have been used:
1. Dagger2 for dependency injections
2. Glide for faster image lazy loadings
3. Retrofit for network calls
4. AppCompat and App design libraries
5. App support libraries for the snackbar, constraint layout and other views
* No database is involved
* To run the tests, manually right click on the package inside test folder and click on run tests, jacoco can be integrated to allow running of tests using gradlew jacocoTestReport
* Download this source code and run it on a device 

### Who do I talk to? ###

* Repo owner or admin: Dhara Shah
* Contact me at sdhara2@hotmail.com