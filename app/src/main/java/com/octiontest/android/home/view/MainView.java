package com.octiontest.android.home.view;

import android.support.annotation.NonNull;

import com.octiontest.android.home.model.MainModelAdapter;

public interface MainView {
    void initViews(@NonNull MainModelAdapter modelAdapter);
}
