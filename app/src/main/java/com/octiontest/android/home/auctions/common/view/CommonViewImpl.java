package com.octiontest.android.home.auctions.common.view;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.octiontest.android.R;
import com.octiontest.android.core.view.OctionSnackBar;

public abstract class CommonViewImpl implements CommonView {
    @NonNull
    private final Activity activity;
    protected View progress;
    protected View rootView;

    public CommonViewImpl(@NonNull final Activity activity) {
        this.activity = activity;
    }

    @Override
    public void initViews() {
        progress = activity.findViewById(R.id.progress);
        rootView = activity.findViewById(R.id.root_view);
    }

    @Override
    public void showProgress() {
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progress.setVisibility(View.GONE);
    }

    @Override
    public void showError(@NonNull final String errorMessage) {
        OctionSnackBar.make(rootView, errorMessage).show();
    }

    @NonNull
    public Activity getActivity() {
        return activity;
    }
}
