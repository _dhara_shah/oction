package com.octiontest.android.home.auctions.upcoming.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.octiontest.android.R;
import com.octiontest.android.core.utils.OctionRes;
import com.octiontest.android.home.auctions.common.model.AuctionModelAdapter;

final class UpcomingAuctionsAdapter extends RecyclerView.Adapter<UpcomingAuctionsAdapter.UpcomingAuctionVH> {
    private final AuctionModelAdapter modelAdapter;

    public static UpcomingAuctionsAdapter create(@NonNull final AuctionModelAdapter modelAdapter) {
        return new UpcomingAuctionsAdapter(modelAdapter);
    }

    private UpcomingAuctionsAdapter(@NonNull final AuctionModelAdapter modelAdapter) {
        this.modelAdapter = modelAdapter;
    }

    @Override
    public UpcomingAuctionVH onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_upcoming_auctions_individual, parent, false);
        return new UpcomingAuctionVH(itemView);
    }

    @Override
    public void onBindViewHolder(final UpcomingAuctionVH holder, final int position) {
        bindUpcomingAuction(holder, position);
    }

    @Override
    public int getItemCount() {
        return modelAdapter.getCount();
    }

    private void bindUpcomingAuction(@NonNull final UpcomingAuctionVH vh, final int position) {
        vh.txtTitle.setText(modelAdapter.getTitle(position));
        vh.txtRetailPrice.setText(OctionRes.getString(R.string.retail_price,
                modelAdapter.getRetailPrice(position)));
        Glide.with(vh.itemView.getContext()).load(modelAdapter.getImage(position)).into(vh.imgProduct);
        vh.txtStartsTime.setText(modelAdapter.getStartsIn(position));
    }

    static class UpcomingAuctionVH extends RecyclerView.ViewHolder {
        TextView txtTitle;
        TextView txtRetailPrice;
        TextView txtStartsTime;
        ImageView imgProduct;

        UpcomingAuctionVH(final View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.txt_title);
            imgProduct = (ImageView) itemView.findViewById(R.id.image_product);
            txtRetailPrice = (TextView) itemView.findViewById(R.id.txt_retail_price);
            txtStartsTime = (TextView) itemView.findViewById(R.id.txt_start_time);
        }
    }
}
