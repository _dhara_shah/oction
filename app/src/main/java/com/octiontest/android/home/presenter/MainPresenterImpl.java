package com.octiontest.android.home.presenter;

import android.support.annotation.NonNull;

import com.octiontest.android.R;
import com.octiontest.android.core.utils.OctionRes;
import com.octiontest.android.home.model.MainModelAdapterImpl;
import com.octiontest.android.home.router.MainRouter;
import com.octiontest.android.home.view.MainView;

public class MainPresenterImpl implements MainPresenter {
    private final MainView view;
    private final MainRouter router;

    public MainPresenterImpl(@NonNull final MainView view,
                             @NonNull final MainRouter router) {
        this.view = view;
        this.router = router;
    }

    @Override
    public void handleOnViewCreated() {
        view.initViews(new MainModelAdapterImpl(OctionRes.getStringArray(R.array.tab_strips)));
    }

    @Override
    public boolean handleOnBackPressed() {
        router.close();
        return true;
    }
}
