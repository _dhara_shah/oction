package com.octiontest.android.home.router;

import android.app.Activity;
import android.support.annotation.NonNull;

public class MainRouterImpl implements MainRouter {
    @NonNull
    private final Activity activity;

    public MainRouterImpl(@NonNull final Activity activity) {
        this.activity = activity;
    }

    @Override
    public void close() {
        activity.onBackPressed();
    }
}
