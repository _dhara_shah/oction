package com.octiontest.android.home.auctions.common.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

import com.octiontest.android.core.network.entity.auctions.AuctionResponse;
import com.octiontest.android.core.utils.StringUtils;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AuctionModelAdapterImpl implements AuctionModelAdapter {
    private final List<AuctionResponse> auctionList;

    @VisibleForTesting
    AuctionType type;

    public static AuctionModelAdapterImpl create(@NonNull final List<AuctionResponse> auctionList,
                                          final AuctionType type) {
        return new AuctionModelAdapterImpl(auctionList, type);
    }

    private AuctionModelAdapterImpl(@NonNull final List<AuctionResponse> auctionList,
                                    final AuctionType type) {
        this.auctionList = auctionList;
        this.type = type;
    }

    @Override
    public int getCount() {
        return auctionList.size();
    }

    @NonNull
    @Override
    public String getTitle(final int position) {
        return auctionList.isEmpty() ? "" : auctionList.get(position).getAuction().getTitle();
    }

    @NonNull
    @Override
    public String getDescription(final int position) {
        return auctionList.isEmpty() ? "" : auctionList.get(position).getAuction().getDescription();
    }

    @Override
    public String getRetailPrice(final int position) {
        final DecimalFormat formatter = new DecimalFormat("#.##");
        String retailPrice = StringUtils.ZERO;

        if (!auctionList.isEmpty()) {
            retailPrice = StringUtils.getNonNullNumberValue(auctionList.get(position).getAuction().getProductPrice());
        }
        return getProductCurrency(position) + formatter.format(Double.parseDouble(retailPrice));
    }

    @Override
    public double getMinimumPrice(final int position) {
        final DecimalFormat formatter = new DecimalFormat("#.##");
        String startPrice = StringUtils.ZERO;

        if (!auctionList.isEmpty()) {
            startPrice = StringUtils.getNonNullNumberValue(auctionList.get(position).getAuction().getMinimumPrice());
        }
        return Double.parseDouble(formatter.format(Double.parseDouble(startPrice)));
    }

    @Nullable
    @Override
    public String getImage(final int position) {
        return auctionList.isEmpty() ? "" : auctionList.get(position).getMediaList().get(0).getMedia();
    }

    @Override
    public boolean isACurrentAuction() {
        return type == AuctionType.AUCTION_CURRENT;
    }

    @Override
    public String getCurrency(final int position) {
        if (!auctionList.isEmpty()) {
            return auctionList.get(position).getAuction().getCurrency();
        }
        return "";
    }

    @Override
    public double getBidPrice(final int position) {
        final DecimalFormat formatter = new DecimalFormat("#.##");
        String bidIncrementPrice = StringUtils.ZERO;

        if (!auctionList.isEmpty()) {
            bidIncrementPrice = StringUtils.getNonNullNumberValue(auctionList.get(position).getAuction().getPrice());
        }
        return Double.valueOf(formatter.format(Double.parseDouble(bidIncrementPrice)));
    }

    @Override
    public double calculateSeconds(final int progress) {
        final DecimalFormat formatter = new DecimalFormat("#.###");
        return Double.parseDouble(formatter.format(((float) progress) * 10 / 100));
    }

    @NonNull @VisibleForTesting
    String getProductCurrency(final int position) {
        if (!auctionList.isEmpty()) {
            return auctionList.get(position).getAuction().getProductCurrency();
        }
        return "";
    }

    @Override
    @NonNull
    public String getStartsIn(final int position) {
        final long unixDivision = 1000L;
        final long currentDate = System.currentTimeMillis() / unixDivision;
        final long actualStartDate = getUnixStartTime(position);
        return getDifference(currentDate, actualStartDate);
    }

    private long getUnixStartTime(final int position) {
        if (!auctionList.isEmpty()) {
            final String startUnixTime = StringUtils.getNonNullNumberValue(auctionList.get(position)
                    .getAuction().getStartTimeUnix());
            return Long.parseLong(startUnixTime);
        }
        return System.currentTimeMillis() + 1;
    }

    @NonNull
    private static String getDifference(final long currentDate, final long actualStartDate) {
        final Date dtCurrentDate = new Date(currentDate * 1000L);
        final Date dtActualStartDate = new Date(actualStartDate * 1000L);
        return getDiff(dtCurrentDate, dtActualStartDate);
    }

    private static String getDiff(final Date currentDate, final Date actualStartDate){
        //milliseconds
        long different = actualStartDate.getTime() - currentDate.getTime();
        final long secondsInMilli = 1000;
        final long minutesInMilli = secondsInMilli * 60;
        final long hoursInMilli = minutesInMilli * 60;
        final long daysInMilli = hoursInMilli * 24;

        final long elapsedDays = different / daysInMilli;
        different %= daysInMilli;

        final long elapsedHours = different / hoursInMilli;
        different %= hoursInMilli;

        final long elapsedMinutes = different / minutesInMilli;
        different %= minutesInMilli;

        final long elapsedSeconds = different / secondsInMilli;

        return String.format(Locale.ENGLISH, "%dd, %dhr, %dmin, %ds",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);
    }
}
