package com.octiontest.android.home.view;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.octiontest.android.home.auctions.CurrentAuctionsFragment;
import com.octiontest.android.home.auctions.UpcomingAuctionsFragment;
import com.octiontest.android.home.model.MainModelAdapter;

class MainTabAdapter extends FragmentPagerAdapter {
    @NonNull
    private final MainModelAdapter modelAdapter;

    MainTabAdapter(final FragmentManager fm, @NonNull final MainModelAdapter modelAdapter) {
        super(fm);
        this.modelAdapter = modelAdapter;
    }

    @Override
    public Fragment getItem(final int position) {
        if (position == 1) {
            return UpcomingAuctionsFragment.newInstance();
        }
        return CurrentAuctionsFragment.newInstance();
    }

    @Override
    public int getCount() {
        return modelAdapter.getCount();
    }

    @Override
    public CharSequence getPageTitle(final int position) {
        return modelAdapter.getTitle(position);
    }
}
