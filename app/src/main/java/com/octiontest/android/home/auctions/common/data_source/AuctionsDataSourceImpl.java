package com.octiontest.android.home.auctions.common.data_source;

import android.support.annotation.NonNull;

import com.octiontest.android.OctionApp;
import com.octiontest.android.core.network.api.Listener;
import com.octiontest.android.core.network.api.ServiceError;
import com.octiontest.android.core.network.entity.auctions.AuctionResponse;
import com.octiontest.android.di.OctionInjector;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AuctionsDataSourceImpl implements AuctionsDataSource {
    @Inject
    Retrofit retrofit;

    public AuctionsDataSourceImpl() {
        OctionInjector.from(OctionApp.INSTANCE).inject(this);
    }

    @Override
    public void getCurrentAuctions(@NonNull final Listener<List<AuctionResponse>> listener) {
        final Call<List<AuctionResponse>> auctions = retrofit.create(RestApiSource.class).getCurrentAuctions();
        auctions.enqueue(new Callback<List<AuctionResponse>>() {
            @Override
            public void onResponse(final Call<List<AuctionResponse>> call, final Response<List<AuctionResponse>> response) {
                listener.onSuccess(response.body());
            }

            @Override
            public void onFailure(final Call<List<AuctionResponse>> call, final Throwable t) {
                listener.onError(new ServiceError(t));
            }
        });
    }
}
