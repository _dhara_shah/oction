package com.octiontest.android.home.auctions.current.view;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.octiontest.android.R;
import com.octiontest.android.core.utils.OctionRes;
import com.octiontest.android.home.auctions.common.model.AuctionModelAdapter;
import com.octiontest.android.home.utils.SimpleSpanBuilder;

import java.text.DecimalFormat;

final class CurrentAuctionAdapter extends RecyclerView.Adapter<CurrentAuctionAdapter.CurrentAuctionVH> {
    private final AuctionModelAdapter modelAdapter;

    public static CurrentAuctionAdapter create(@NonNull final AuctionModelAdapter modelAdapter) {
        return new CurrentAuctionAdapter(modelAdapter);
    }

    private CurrentAuctionAdapter(@NonNull final AuctionModelAdapter modelAdapter) {
        this.modelAdapter = modelAdapter;
    }

    @Override
    public CurrentAuctionVH onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_current_auctions_individual, parent, false);
        return new CurrentAuctionVH(itemView);
    }

    @Override
    public void onBindViewHolder(final CurrentAuctionVH holder, final int position) {
        bindCurrentAuction(holder, position);
    }

    @Override
    public int getItemCount() {
        return modelAdapter.getCount();
    }

    private void bindCurrentAuction(@NonNull final CurrentAuctionVH vh, final int position) {
        vh.txtTitle.setText(modelAdapter.getTitle(position));
        vh.txtRetailPrice.setText(OctionRes.getString(R.string.retail_price,
                modelAdapter.getRetailPrice(position)));
        Glide.with(vh.itemView.getContext()).load(modelAdapter.getImage(position)).into(vh.imgProduct);
        vh.txtPrice.setText(modelAdapter.getCurrency(position) + ' ' + String.valueOf(modelAdapter.getMinimumPrice(position)));
        setTimer(vh, position);
    }

    private static void decorateText(@NonNull final CurrentAuctionVH vh, final double remainingSeconds) {
        final float relativeSpanSeconds = 1.4f;
        final float relativeSpanRemaining = 1.2f;
        final String s = "s";
        final String strRemainingSeconds = String.valueOf(remainingSeconds);
        final String pluralSeconds = strRemainingSeconds + s;
        final SimpleSpanBuilder spanBuilder = new SimpleSpanBuilder();
        spanBuilder.append(pluralSeconds,
                new ForegroundColorSpan(ContextCompat.getColor(vh.itemView.getContext(), R.color.colorRed)),
                new RelativeSizeSpan(relativeSpanSeconds));
        spanBuilder.append(OctionRes.getString(R.string.seconds_remaining), new RelativeSizeSpan(relativeSpanRemaining));
        vh.txtSeconds.setText(spanBuilder.build());
    }

    private void setTimer(@NonNull final CurrentAuctionVH vh, final int position) {
        final int tenSeconds = 10 * 1000;

        final ObjectAnimator animation = ObjectAnimator.ofInt(vh.progressBar, "progress", 100, 0);
        animation.setDuration(tenSeconds);
        animation.setRepeatCount(ObjectAnimator.INFINITE);
        animation.setInterpolator(new DecelerateInterpolator());
        animation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(final Animator animator) {
                decorateText(vh, vh.progressBar.getProgress());
            }

            @Override
            public void onAnimationEnd(final Animator animator) {
                //do something when the countdown is complete
            }

            @Override
            public void onAnimationCancel(final Animator animator) {
            }

            @Override
            public void onAnimationRepeat(final Animator animator) {
                setPrice(vh, position);
                vh.progressBar.setProgress(tenSeconds);
            }

            void setPrice(@NonNull final CurrentAuctionVH vh, final int position) {
                final double price = Double.parseDouble(vh.txtPrice.getText().toString().split(" ")[1]);
                final double newPrice = price + modelAdapter.getBidPrice(position);

                final DecimalFormat formatter = new DecimalFormat("#.##");
                vh.txtPrice.setText(modelAdapter.getCurrency(position) + ' ' + formatter.format(newPrice));
            }
        });
        animation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(final ValueAnimator animation) {
                decorateText(vh, modelAdapter.calculateSeconds(vh.progressBar.getProgress()));
            }
        });
        animation.start();
    }

    static class CurrentAuctionVH extends RecyclerView.ViewHolder {
        TextView txtTitle;
        TextView txtRetailPrice;
        TextView txtSeconds;
        TextView txtPrice;
        ProgressBar progressBar;
        ImageView imgProduct;

        CurrentAuctionVH(final View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.txt_name);
            imgProduct = (ImageView) itemView.findViewById(R.id.image_item);
            txtRetailPrice = (TextView) itemView.findViewById(R.id.txt_retail_price);
            txtSeconds = (TextView) itemView.findViewById(R.id.txt_seconds);
            txtPrice = (TextView) itemView.findViewById(R.id.txt_price);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar_timer);
        }
    }
}
