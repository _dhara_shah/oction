package com.octiontest.android.home.auctions.current.view;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ProgressBar;

import com.octiontest.android.R;
import com.octiontest.android.core.network.entity.auctions.AuctionResponse;
import com.octiontest.android.home.auctions.common.model.AuctionModelAdapter;
import com.octiontest.android.home.auctions.common.model.AuctionModelAdapterImpl;
import com.octiontest.android.home.auctions.common.model.AuctionType;
import com.octiontest.android.home.auctions.common.view.CommonViewImpl;

import java.util.Collections;

public class CurrentAuctionsViewImpl extends CommonViewImpl {
    private RecyclerView recyclerView;
    public static CurrentAuctionsViewImpl from(@NonNull final Activity activity) {
        return new CurrentAuctionsViewImpl(activity);
    }

    private CurrentAuctionsViewImpl(@NonNull final Activity activity) {
        super(activity);
    }

    @Override
    public void initViews() {
        super.initViews();

        progress = getActivity().findViewById(R.id.progress_current);
        recyclerView = (RecyclerView) getActivity().findViewById(R.id.rv_current_list);

        final LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);

        final AuctionModelAdapter modelAdapter = AuctionModelAdapterImpl.create(Collections.<AuctionResponse>emptyList(),
                AuctionType.AUCTION_CURRENT);
        final CurrentAuctionAdapter adapter = CurrentAuctionAdapter.create(modelAdapter);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void updateData(@NonNull final AuctionModelAdapter modelAdapter) {
        final CurrentAuctionAdapter adapter = CurrentAuctionAdapter.create(modelAdapter);
        recyclerView.swapAdapter(adapter, false);
    }
}
