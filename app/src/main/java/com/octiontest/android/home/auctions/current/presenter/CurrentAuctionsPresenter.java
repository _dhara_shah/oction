package com.octiontest.android.home.auctions.current.presenter;

public interface CurrentAuctionsPresenter {
    void handleOnViewCreated();
}
