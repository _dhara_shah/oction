package com.octiontest.android.home.auctions.upcoming.presenter;

public interface UpcomingAuctionsPresenter {
    void handleOnViewCreated();
}
