package com.octiontest.android.home.auctions.upcoming.model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.octiontest.android.core.network.entity.auctions.AuctionResponse;

import java.lang.reflect.Type;
import java.util.List;

public class UpcomingAuctionsMock {
    public static List<AuctionResponse> mock() {
        return _mock();
    }

    private static List<AuctionResponse> _mock() {
        final Gson gson = new Gson();
        final Type collectionType = new TypeToken<List<AuctionResponse>>() {}.getType();
        return gson.fromJson(UPCOMING_AUCTIONS_MOCK, collectionType);
    }

    private static final String UPCOMING_AUCTIONS_MOCK = "[\n" +
            "{\n" +
            "\"auction\": {\n" +
            "\"user_name\": \"\",\n" +
            "\"name\": \"\",\n" +
            "\"profileImage\": \"/media/0_yZfGT081g9yWCRPsLtskuDxpBHS3IQxvPjZB3mcl.jpeg\",\n" +
            "\"userId\": \"0\",\n" +
            "\"title\": \"立刻更新Oction啊！｜THIS IS A DUMMY UPCOMING AUCTION\",\n" +
            "\"description\": \"Oction今天更新，讓用家可以更投入在Oction 的暢快購物樂趣中。因此，希望還未更新的用家盡快將Oction更新為最新版本，以便享受一個更舒服的購物體驗。大家盡快更新最新版本Oction啦！\\n\\nNew update! This new release brings smoother, more awesome auctions for your enjoyment - alongside more great products and features! Please update your app now via the App Store or Google Play Store to continue participating in our auctions.\",\n" +
            "\"productPrice\": \"1000000\",\n" +
            "\"product_id\": \"100000\",\n" +
            "\"productCurrency\": \"HKD\",\n" +
            "\"auctionId\": \"99999\",\n" +
            "\"startingPrice\": \"0\",\n" +
            "\"currency\": \"HKD\",\n" +
            "\"price\": \"0.01\",\n" +
            "\"unique_id\": \"62f23c06c358bfca44343\",\n" +
            "\"start_time\": \"2018-08-17 01:00:00\",\n" +
            "\"start_time_unix\": \"1534467600\",\n" +
            "\"end_time\": \"2018-08-16 01:00:00\",\n" +
            "\"end_time_unix\": \"1534381200\",\n" +
            "\"minimumPrice\": \"4\",\n" +
            "\"bidCount\": \"10000000\",\n" +
            "\"winnerBidCount\": \"0\",\n" +
            "\"bidderCount\": \"0\",\n" +
            "\"auctionsHeldCount\": \"0\"\n" +
            "},\n" +
            "\"media\": [\n" +
            "{\n" +
            "\"media\": \"/media/0_PI9c80peFA1S96HgwBd7DUjbnxJY0KpuhOdgXjYf.jpeg\"\n" +
            "}\n" +
            "]\n" +
            "}\n" +
            "]";
}
