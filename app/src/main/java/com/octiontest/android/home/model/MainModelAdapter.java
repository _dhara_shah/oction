package com.octiontest.android.home.model;

import android.support.annotation.NonNull;

public interface MainModelAdapter {
    int getCount();

    @NonNull String getTitle(int position);
}
