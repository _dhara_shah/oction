package com.octiontest.android.home.auctions.common.presenter;

import android.support.annotation.NonNull;

import com.octiontest.android.home.auctions.common.model.AuctionsListener;
import com.octiontest.android.home.auctions.common.view.CommonView;

public abstract class AbstractPresenterImpl implements AuctionsListener {
    private final CommonView view;

    public AbstractPresenterImpl(@NonNull final CommonView view) {
        this.view = view;
    }

    @Override
    public void onAuctionsLoadError(@NonNull final String errorMessage) {
        view.hideProgress();
        view.showError(errorMessage);
    }

    protected CommonView getView() {
        return view;
    }
}
