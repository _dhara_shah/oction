package com.octiontest.android.home.view;

import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.octiontest.android.R;
import com.octiontest.android.home.model.MainModelAdapter;

public class

MainViewImpl implements MainView {
    @NonNull
    private final FragmentActivity activity;

    public MainViewImpl(@NonNull final FragmentActivity activity) {
        this.activity = activity;
    }

    @Override
    public void initViews(@NonNull final MainModelAdapter mainModelAdapter) {
        final Toolbar toolbar = (Toolbar) activity.findViewById(R.id.toolbar);
        ((AppCompatActivity)activity).setSupportActionBar(toolbar);

        final MainTabAdapter adapter = new MainTabAdapter(activity.getSupportFragmentManager(), mainModelAdapter);
        final ViewPager viewPager = (ViewPager) activity.findViewById(R.id.view_pager);
        viewPager.setAdapter(adapter);

        final TabLayout tabLayout = (TabLayout) activity.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        viewPager.setOffscreenPageLimit(1);
    }
}
