package com.octiontest.android.home.auctions.common.data_source;

import com.octiontest.android.core.network.api.Api;
import com.octiontest.android.core.network.entity.auctions.AuctionResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RestApiSource {
    @GET(Api.AUCTIONS.GET_AUCTIONS)
    Call<List<AuctionResponse>> getCurrentAuctions();
}
