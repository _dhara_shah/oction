package com.octiontest.android.home.auctions.current.presenter;

import android.support.annotation.NonNull;

import com.octiontest.android.OctionApp;
import com.octiontest.android.di.OctionInjector;
import com.octiontest.android.home.auctions.common.model.AuctionModelAdapter;
import com.octiontest.android.home.auctions.common.model.AuctionModelAdapterImpl;
import com.octiontest.android.home.auctions.common.model.AuctionType;
import com.octiontest.android.home.auctions.common.model.AuctionsInteractor;
import com.octiontest.android.home.auctions.common.presenter.AbstractPresenterImpl;
import com.octiontest.android.home.auctions.common.view.CommonView;

import javax.inject.Inject;


public class CurrentAuctionsPresenterImpl extends AbstractPresenterImpl implements CurrentAuctionsPresenter {
    @Inject
    AuctionsInteractor interactor;

    public static CurrentAuctionsPresenterImpl from(@NonNull final CommonView view) {
        return new CurrentAuctionsPresenterImpl(view);
    }

    private CurrentAuctionsPresenterImpl(@NonNull final CommonView view) {
        super(view);
        OctionInjector.from(OctionApp.INSTANCE).inject(this);
    }

    @Override
    public void handleOnViewCreated() {
        getView().initViews();
        getView().showProgress();
        interactor.loadAuctions(this);
    }

    @Override
    public void onAuctionsLoadSuccess() {
        getView().hideProgress();

        final AuctionModelAdapter modelAdapter = AuctionModelAdapterImpl.create(interactor.getAuctionList(),
                AuctionType.AUCTION_CURRENT);
        getView().updateData(modelAdapter);
    }
}
