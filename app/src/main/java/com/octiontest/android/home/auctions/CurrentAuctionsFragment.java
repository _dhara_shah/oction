package com.octiontest.android.home.auctions;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.octiontest.android.R;
import com.octiontest.android.home.auctions.common.view.CommonView;
import com.octiontest.android.home.auctions.current.presenter.CurrentAuctionsPresenter;
import com.octiontest.android.home.auctions.current.presenter.CurrentAuctionsPresenterImpl;
import com.octiontest.android.home.auctions.current.view.CurrentAuctionsViewImpl;

public class CurrentAuctionsFragment extends Fragment {
    public static CurrentAuctionsFragment newInstance() {
        return new CurrentAuctionsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater,
                             @Nullable final ViewGroup container,
                             @Nullable final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_current, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final CommonView currentAuctionsView = CurrentAuctionsViewImpl.from(getActivity());
        final CurrentAuctionsPresenter presenter = CurrentAuctionsPresenterImpl.from(currentAuctionsView);
        presenter.handleOnViewCreated();
    }
}
