package com.octiontest.android.home.auctions.common.data_source;

import android.support.annotation.NonNull;

import com.octiontest.android.core.network.api.Listener;
import com.octiontest.android.core.network.entity.auctions.AuctionResponse;

import java.util.List;

public interface AuctionsDataSource {
    void getCurrentAuctions(@NonNull Listener<List<AuctionResponse>> listener);
}
