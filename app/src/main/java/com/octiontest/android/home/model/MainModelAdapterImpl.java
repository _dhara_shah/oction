package com.octiontest.android.home.model;

import android.support.annotation.NonNull;

public class MainModelAdapterImpl implements MainModelAdapter {
    @NonNull
    private final String[] pageTitleArray;

    public MainModelAdapterImpl(@NonNull final String[] pageTitleArray) {
        this.pageTitleArray = pageTitleArray;
    }

    @Override
    public int getCount() {
        return pageTitleArray.length;
    }

    @NonNull
    @Override
    public String getTitle(final int position) {
        return pageTitleArray[position];
    }
}
