package com.octiontest.android.home.auctions.upcoming.view;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.octiontest.android.R;
import com.octiontest.android.core.network.entity.auctions.AuctionResponse;
import com.octiontest.android.home.auctions.common.model.AuctionModelAdapter;
import com.octiontest.android.home.auctions.common.model.AuctionModelAdapterImpl;
import com.octiontest.android.home.auctions.common.model.AuctionType;
import com.octiontest.android.home.auctions.common.view.CommonViewImpl;

import java.util.Collections;

public class UpcomingAuctionsViewImpl extends CommonViewImpl {
    private RecyclerView recyclerView;

    public static UpcomingAuctionsViewImpl from(@NonNull final Activity activity) {
        return new UpcomingAuctionsViewImpl(activity);
    }

    private UpcomingAuctionsViewImpl(@NonNull final Activity activity) {
        super(activity);
    }

    @Override
    public void initViews() {
        super.initViews();
        recyclerView = (RecyclerView) getActivity().findViewById(R.id.rv_upcoming_list);

        final LinearLayoutManager manager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(false);

        final AuctionModelAdapter modelAdapter = AuctionModelAdapterImpl.create(Collections.<AuctionResponse>emptyList(),
                AuctionType.AUCTION_UPCOMING);
        final UpcomingAuctionsAdapter adapter = UpcomingAuctionsAdapter.create(modelAdapter);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void updateData(@NonNull final AuctionModelAdapter modelAdapter) {
        final UpcomingAuctionsAdapter adapter = UpcomingAuctionsAdapter.create(modelAdapter);
        recyclerView.swapAdapter(adapter, false);
    }
}
