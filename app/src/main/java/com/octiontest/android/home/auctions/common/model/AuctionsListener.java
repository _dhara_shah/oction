package com.octiontest.android.home.auctions.common.model;

import android.support.annotation.NonNull;

public interface AuctionsListener {
    void onAuctionsLoadSuccess();

    void onAuctionsLoadError(@NonNull String errorMessage);
}
