package com.octiontest.android.home.auctions;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.octiontest.android.R;
import com.octiontest.android.home.auctions.common.view.CommonView;
import com.octiontest.android.home.auctions.upcoming.presenter.UpcomingAuctionsPresenter;
import com.octiontest.android.home.auctions.upcoming.presenter.UpcomingAuctionsPresenterImpl;
import com.octiontest.android.home.auctions.upcoming.view.UpcomingAuctionsViewImpl;

public class UpcomingAuctionsFragment extends Fragment {
    public static UpcomingAuctionsFragment newInstance() {
        return new UpcomingAuctionsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater,
                             @Nullable final ViewGroup container,
                             @Nullable final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_upcoming, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        super.onViewCreated(view, savedInstanceState);
        final CommonView upcomingAuctionsView = UpcomingAuctionsViewImpl.from(getActivity());
        final UpcomingAuctionsPresenter presenter = UpcomingAuctionsPresenterImpl.from(upcomingAuctionsView);
        presenter.handleOnViewCreated();
    }
}
