package com.octiontest.android.home.auctions.common.model;

import android.support.annotation.NonNull;

import com.octiontest.android.OctionApp;
import com.octiontest.android.core.network.api.Listener;
import com.octiontest.android.core.network.api.ServiceError;
import com.octiontest.android.core.network.entity.auctions.AuctionResponse;
import com.octiontest.android.di.OctionInjector;
import com.octiontest.android.home.auctions.common.data_source.AuctionsDataSource;
import com.octiontest.android.home.auctions.upcoming.model.UpcomingAuctionsMock;

import java.util.List;

import javax.inject.Inject;

public class AuctionsInteractorImpl implements AuctionsInteractor {
    private List<AuctionResponse> auctionList;

    @Inject
    AuctionsDataSource dataSource;

    public AuctionsInteractorImpl() {
        OctionInjector.from(OctionApp.INSTANCE).inject(this);
    }

    @Override
    public void loadAuctions(@NonNull final AuctionsListener listener) {
        dataSource.getCurrentAuctions(new Listener<List<AuctionResponse>>() {
            @Override
            public void onSuccess(final List<AuctionResponse> response) {
                auctionList = response;
                listener.onAuctionsLoadSuccess();
            }

            @Override
            public void onError(final ServiceError error) {
                listener.onAuctionsLoadError(error.getErrorMessage());
            }
        });
    }

    @Override
    public void loadUpcomingAuctions(@NonNull final AuctionsListener listener) {
        auctionList = UpcomingAuctionsMock.mock();
        listener.onAuctionsLoadSuccess();
    }

    @Override
    public List<AuctionResponse> getAuctionList() {
        return auctionList;
    }
}
