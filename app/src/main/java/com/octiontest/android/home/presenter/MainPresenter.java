package com.octiontest.android.home.presenter;

public interface MainPresenter {
    void handleOnViewCreated();

    boolean handleOnBackPressed();

    MainPresenter STUB = new MainPresenter() {
        @Override
        public void handleOnViewCreated() {

        }

        @Override
        public boolean handleOnBackPressed() {
            return false;
        }
    };
}
