package com.octiontest.android.home;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.octiontest.android.R;
import com.octiontest.android.home.presenter.MainPresenter;
import com.octiontest.android.home.presenter.MainPresenterImpl;
import com.octiontest.android.home.router.MainRouter;
import com.octiontest.android.home.router.MainRouterImpl;
import com.octiontest.android.home.view.MainView;
import com.octiontest.android.home.view.MainViewImpl;

public class MainActivity extends AppCompatActivity {
    private MainPresenter presenter = MainPresenter.STUB;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final MainView view = new MainViewImpl(this);
        final MainRouter router = new MainRouterImpl(this);
        presenter = new MainPresenterImpl(view, router);
        presenter.handleOnViewCreated();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        presenter.handleOnBackPressed();
    }
}
