package com.octiontest.android.home.auctions.common.model;

import android.support.annotation.NonNull;

import com.octiontest.android.core.network.entity.auctions.AuctionResponse;

import java.util.List;

public interface AuctionsInteractor {
    void loadAuctions(@NonNull AuctionsListener listener);

    void loadUpcomingAuctions(@NonNull AuctionsListener listener);

    List<AuctionResponse> getAuctionList();
}
