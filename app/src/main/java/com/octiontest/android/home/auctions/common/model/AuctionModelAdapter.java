package com.octiontest.android.home.auctions.common.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public interface AuctionModelAdapter {
    int getCount();

    @NonNull
    String getTitle(int position);

    @NonNull
    String getDescription(int position);

    String getRetailPrice(int position);

    double getMinimumPrice(int position);

    String getCurrency(int position);

    double getBidPrice(int position);

    double calculateSeconds(int progress);

    @Nullable
    String getImage(int position);

    boolean isACurrentAuction();

    @NonNull
    String getStartsIn(int position);
}
