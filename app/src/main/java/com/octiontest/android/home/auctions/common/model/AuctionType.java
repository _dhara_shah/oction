package com.octiontest.android.home.auctions.common.model;

public enum AuctionType {
    AUCTION_UPCOMING,
    AUCTION_CURRENT
}
