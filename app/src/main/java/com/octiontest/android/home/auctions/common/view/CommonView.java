package com.octiontest.android.home.auctions.common.view;

import android.support.annotation.NonNull;

import com.octiontest.android.home.auctions.common.model.AuctionModelAdapter;

public interface CommonView {
    void initViews();

    void showProgress();

    void hideProgress();

    void updateData(@NonNull AuctionModelAdapter modelAdapter);

    void showError(@NonNull String errorMessage);
}
