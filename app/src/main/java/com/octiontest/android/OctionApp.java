package com.octiontest.android;

import android.app.Application;
import android.content.Context;

import com.octiontest.android.core.di.DaggerCoreComponent;
import com.octiontest.android.core.network.api.Api;
import com.octiontest.android.core.utils.AppInitImpl;
import com.octiontest.android.dagger2.AppComponent;
import com.octiontest.android.dagger2.AppModule;
import com.octiontest.android.dagger2.ApplicationModuleImpl;
import com.octiontest.android.dagger2.CoreModuleImpl;
import com.octiontest.android.dagger2.DaggerAppComponent;
import com.octiontest.android.dagger2.DaggerOctionComponent;
import com.octiontest.android.dagger2.OctionComponent;
import com.octiontest.android.dagger2.OctionModuleImpl;
import com.octiontest.android.core.di.ApplicationModule;
import com.octiontest.android.core.di.CoreComponent;
import com.octiontest.android.core.di.CoreComponentProvider;
import com.octiontest.android.core.di.CoreModule;
import com.octiontest.android.core.di.NetModule;
import com.octiontest.android.di.OctionComponentProvider;

public class OctionApp extends Application implements CoreComponentProvider, OctionComponentProvider {
        public static OctionApp INSTANCE;
        public static AppComponent INJECTOR;
        public static OctionComponent OCTION_INJECTOR;
        private static volatile CoreComponent CORE_INJECTOR;

        @Override
        public void onCreate () {
        super.onCreate();

        INSTANCE = this;

        final ApplicationModule applicationModule = new ApplicationModuleImpl();
        final CoreModule coreModule = new CoreModuleImpl(this);
        final NetModule netModule = new NetModule(Api.getHost());

        CORE_INJECTOR = DaggerCoreComponent.builder()
                .coreModule(coreModule)
                .netModule(netModule)
                .applicationModule(applicationModule)
                .build();

        INJECTOR = DaggerAppComponent.builder()
                .coreComponent(CORE_INJECTOR)
                .appModule(new AppModule(this))
                .build();


        OCTION_INJECTOR = DaggerOctionComponent.builder()
                .coreComponent(CORE_INJECTOR)
                .octionModule(new OctionModuleImpl())
                .build();


        CORE_INJECTOR.getOctionLog();

        new AppInitImpl().start();
    }

        @Override
        public CoreComponent getCoreComponent () {
        return CORE_INJECTOR;
    }

        @Override
        public OctionComponent getOctionComponent ( final Context context) {
            return OCTION_INJECTOR;
    }
}
