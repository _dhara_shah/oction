package com.octiontest.android.dagger2;

import com.octiontest.android.core.di.CoreComponent;
import com.octiontest.android.core.di.ModuleScope;
import com.octiontest.android.home.auctions.common.data_source.AuctionsDataSourceImpl;
import com.octiontest.android.home.auctions.common.model.AuctionsInteractorImpl;
import com.octiontest.android.home.auctions.current.presenter.CurrentAuctionsPresenterImpl;
import com.octiontest.android.home.auctions.upcoming.presenter.UpcomingAuctionsPresenterImpl;

import dagger.Component;

@ModuleScope
@Component(modules = OctionModule.class, dependencies = CoreComponent.class)
public interface OctionComponent {
    void inject(CurrentAuctionsPresenterImpl currentAuctionsPresenter);

    void inject(AuctionsInteractorImpl auctionsInteractor);

    void inject(AuctionsDataSourceImpl auctionsDataSource);

    void inject(UpcomingAuctionsPresenterImpl upcomingAuctionsPresenter);
}