package com.octiontest.android.dagger2;

import com.octiontest.android.core.utils.AppInit;
import com.octiontest.android.core.utils.AppInitImpl;
import com.octiontest.android.core.di.ApplicationModule;

public class ApplicationModuleImpl extends ApplicationModule {
    @Override
    public AppInit provideAppInit() {
        return new AppInitImpl();
    }
}