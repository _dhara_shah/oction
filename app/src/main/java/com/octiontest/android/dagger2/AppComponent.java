package com.octiontest.android.dagger2;

import com.octiontest.android.core.di.CoreComponent;
import com.octiontest.android.core.di.ModuleScope;

import dagger.Component;

@ModuleScope
@Component(modules = AppModule.class, dependencies = CoreComponent.class)
public interface AppComponent {

}