package com.octiontest.android.dagger2;

import com.octiontest.android.home.auctions.common.data_source.AuctionsDataSource;
import com.octiontest.android.home.auctions.common.data_source.AuctionsDataSourceImpl;
import com.octiontest.android.home.auctions.common.model.AuctionsInteractor;
import com.octiontest.android.home.auctions.common.model.AuctionsInteractorImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class OctionModule {
    @Provides
    public AuctionsInteractor provideAuctionsInteractor() {
        return new AuctionsInteractorImpl();
    }

    @Provides
    public AuctionsDataSource provideAuctionsDataSource() {
        return new AuctionsDataSourceImpl();
    }
}