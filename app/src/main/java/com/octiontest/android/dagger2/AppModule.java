package com.octiontest.android.dagger2;

import android.app.Application;

import com.octiontest.android.OctionApp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    OctionApp application;

    public AppModule(OctionApp application) {
        this.application = application;
    }

    @Singleton
    @Provides
    Application provideApplication() {
        return application;
    }
}