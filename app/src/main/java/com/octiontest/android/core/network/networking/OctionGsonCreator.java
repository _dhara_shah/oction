package com.octiontest.android.core.network.networking;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.bind.DateTypeAdapter;

import java.util.Date;

public class OctionGsonCreator {
    public Gson create() {
        return new GsonBuilder().
                registerTypeAdapter(Date.class, new DateTypeAdapter()).
                create();
    }
}


