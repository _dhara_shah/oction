package com.octiontest.android.core.utils;

import android.support.annotation.Nullable;
import android.text.TextUtils;

public class StringUtils {
    public static final String ZERO = "0";

    public static String getNonNullNumberValue(@Nullable final String value) {
        return TextUtils.isEmpty(value) ? ZERO : value;
    }

}
