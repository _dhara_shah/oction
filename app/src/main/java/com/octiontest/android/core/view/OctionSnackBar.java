package com.octiontest.android.core.view;

import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.octiontest.android.R;
import com.octiontest.android.core.utils.OctionRes;

public final class OctionSnackBar {
    private static final int DEFAULT_DURATION = 4000;
    private static final int DEFAULT_MAX_LINES = 4;
    @DimenRes
    private static final int DEFAULT_TEXT_SIZE = R.dimen.text_mid_large;
    @ColorRes
    private static final int DEFAULT_TEXT_COLOR = R.color.colorWhite;
    @ColorRes
    private static final int DEFAULT_BG_COLOR = R.color.colorPrimary;
    @NonNull
    private final Snackbar snackbar;

    private OctionSnackBar(@NonNull final View view, @NonNull final CharSequence message, final int duration) {
        snackbar = Snackbar.make(view, message, duration);
        setBackgroundColor(DEFAULT_BG_COLOR);
        final TextView tv = getTextView();
        tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, OctionRes.getResources().getDimension(DEFAULT_TEXT_SIZE));
        tv.setTextColor(OctionRes.getColor(DEFAULT_TEXT_COLOR));
        tv.setMaxLines(DEFAULT_MAX_LINES);
    }

    public static OctionSnackBar make(@NonNull final View view, @NonNull final String message) {
        return new OctionSnackBar(view, message, DEFAULT_DURATION);
    }

    private OctionSnackBar setBackgroundColor(@ColorRes final int bgColor) {
        getView().setBackgroundColor(OctionRes.getColor(bgColor));
        return this;
    }

    @NonNull
    private View getView() {
        return snackbar.getView();
    }

    @NonNull
    private TextView getTextView() {
        return (TextView) getView().findViewById(android.support.design.R.id.snackbar_text);
    }

    public void show() {
        snackbar.show();
    }
}

