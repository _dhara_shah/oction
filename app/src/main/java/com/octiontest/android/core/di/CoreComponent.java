package com.octiontest.android.core.di;

import com.google.gson.Gson;
import com.octiontest.android.core.utils.AppInit;
import com.octiontest.android.core.utils.OctionLog;

import javax.inject.Singleton;

import dagger.Component;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

@Singleton
@Component(modules = {ApplicationModule.class, CoreModule.class, NetModule.class})
public interface CoreComponent {
    OctionLog getOctionLog();

    Retrofit getRetrofit();

    Gson getGson();

    OkHttpClient getOkHttpClient();

    AppInit getAppInit();

    void inject(OctionLog octionLog);
}
