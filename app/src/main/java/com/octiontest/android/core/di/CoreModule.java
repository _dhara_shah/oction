package com.octiontest.android.core.di;

import android.app.Application;
import android.content.Context;

import com.octiontest.android.core.utils.OctionLog;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class CoreModule {
    private final Context context;

    public CoreModule(final Context context) {
        this.context = context;
    }

    @Singleton
    @Provides
    public OctionLog provideOctionLog() {
        return new OctionLog();
    }

    @Singleton
    @Provides
    Application providesApplication() {
        return (Application) context;
    }
}