package com.octiontest.android.core.utils;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.ArrayRes;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.IntegerRes;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;

import com.octiontest.android.OctionApp;

public class OctionRes {
    /**
     * @param resId The desired String resource identifier
     * @return String the String resource associated with the resource id
     */
    public static String getString(@StringRes final int resId) {
        return getResources().getString(resId);
    }

    /**
     * @param resId      The desired String resource identifier
     * @param formatArgs The format arguments that will be used for substitution.
     * @return String the String resource associated with the resource id
     */
    public static String getString(@StringRes int resId, Object... formatArgs) {
        return getResources().getString(resId, formatArgs);
    }


    /**
     * @param stringArray The desired resource identifier
     * @return String[] the String[] resource associated with the resource id
     */
    public static String[] getStringArray(final int stringArray) {
        return getResources().getStringArray(stringArray);
    }

    /**
     * Return a Resources instance for your application's package.
     */
    public static Resources getResources() {
        return OctionApp.INSTANCE.getResources();
    }

    public static int getColor(@ColorRes final int color) {
        return ContextCompat.getColor(OctionApp.INSTANCE, color);
    }

    //feel free to add here resource specific helper functions
}
