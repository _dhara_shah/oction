package com.octiontest.android.core.network.api;

public abstract class Api {
    private static final String HOST = "https://dev-us-02.oction.co";

    public static String getHost() {
        return HOST;
    }

    public interface AUCTIONS {
        String GET_AUCTIONS = "/api/v1/auctions";
    }
}
