package com.octiontest.android.core.di;

public interface CoreComponentProvider {
    CoreComponent getCoreComponent();
}