package com.octiontest.android.core.network.entity.auctions;

import com.google.gson.annotations.SerializedName;

import java.util.Collections;
import java.util.List;

public class AuctionResponse {
    @SerializedName("auction")
    private Auction auction;
    @SerializedName("media")
    private List<AuctionMedia> mediaList = Collections.emptyList();

    public Auction getAuction() {
        return auction;
    }

    public List<AuctionMedia> getMediaList() {
        return mediaList;
    }
}
