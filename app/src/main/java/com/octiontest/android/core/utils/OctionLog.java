package com.octiontest.android.core.utils;

import android.util.Log;

import com.octiontest.android.BuildConfig;
import com.octiontest.android.OctionApp;
import com.octiontest.android.core.di.CoreInjector;

public class OctionLog {
    public OctionLog() {
        CoreInjector.from(OctionApp.INSTANCE).inject(this);
    }

    public static void v(final String tag, final String msg) {
        if (useLog()) {
            Log.v(String.valueOf(tag), String.valueOf(msg));
        }
        return;
    }

    private static boolean useLog() {
        return BuildConfig.DEBUG;
    }
}
