package com.octiontest.android.core.network.entity.auctions;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Auction implements Serializable {
    @SerializedName("user_name")
    private String  userName;
    private String  name;
    @SerializedName("profile_image")
    private String  profileImage;
    private String  userId;
    private String  title;
    private String  description;
    private String  productPrice;
    private String  productCurrency;
    private String  auctionId;
    private String  startingPrice;
    private String  currency;
    private String  price;
    @SerializedName("start_time")
    private String  startTime;
    @SerializedName("start_time_unix")
    private String  startTimeUnix;
    @SerializedName("end_time")
    private String  endTime;
    @SerializedName("end_time_unix")
    private String  endTimeUnix;
    private String  minimumPrice;
    private String  bidCount;
    private String  winnderBidCount;
    private String  bidderCount;
    private String  auctionsHeldCount;

    public String  getUserName() {
        return userName;
    }

    public String  getName() {
        return name;
    }

    public String  getProfileImage() {
        return profileImage;
    }

    public String  getUserId() {
        return userId;
    }

    public String  getTitle() {
        return title;
    }

    public String  getDescription() {
        return description;
    }

    public String  getProductPrice() {
        return productPrice;
    }

    public String  getProductCurrency() {
        return productCurrency;
    }

    public String  getAuctionId() {
        return auctionId;
    }

    public String  getStartingPrice() {
        return startingPrice;
    }

    public String  getCurrency() {
        return currency;
    }

    public String  getPrice() {
        return price;
    }

    public String  getStartTime() {
        return startTime;
    }

    public String  getStartTimeUnix() {
        return startTimeUnix;
    }

    public String  getEndTime() {
        return endTime;
    }

    public String  getEndTimeUnix() {
        return endTimeUnix;
    }

    public String  getMinimumPrice() {
        return minimumPrice;
    }

    public String  getBidCount() {
        return bidCount;
    }

    public String  getWinnderBidCount() {
        return winnderBidCount;
    }

    public String  getBidderCount() {
        return bidderCount;
    }

    public String  getAuctionsHeldCount() {
        return auctionsHeldCount;
    }
}
