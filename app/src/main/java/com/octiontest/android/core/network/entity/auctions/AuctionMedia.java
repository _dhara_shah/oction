package com.octiontest.android.core.network.entity.auctions;

import com.octiontest.android.core.network.api.Api;

public class AuctionMedia {
    private String media;

    public String getMedia() {
        return Api.getHost() + media;
    }
}
