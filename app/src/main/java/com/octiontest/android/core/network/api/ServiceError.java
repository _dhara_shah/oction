package com.octiontest.android.core.network.api;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.octiontest.android.core.constants.Errors;

public class ServiceError {
    @Nullable
    private final Object response;
    @NonNull
    private final String errorMessage;
    @NonNull
    private final String errorCode;

    public ServiceError(@NonNull final String errorMessage, @NonNull final String errorCode) {
        this.errorMessage = errorMessage;
        this.errorCode = errorCode;
        this.response = null;
    }

    public ServiceError(@NonNull final Throwable t) {
        this.errorMessage = t.getMessage();
        this.errorCode = Errors.CODE_EMPTY;
        this.response = null;
    }

    @NonNull
    public String getErrorMessage() {
        return errorMessage;
    }

    @Override
    public String toString() {
        return "ServiceError{" +
                "errorMessage='" + errorMessage + '\'' +
                ", errorCode='" + errorCode + '\'' +
                '}';
    }

    @Nullable
    public Object getResponse() {
        return response;
    }
}
