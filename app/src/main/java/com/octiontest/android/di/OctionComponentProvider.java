package com.octiontest.android.di;

import android.content.Context;

import com.octiontest.android.dagger2.OctionComponent;

public interface OctionComponentProvider {
    OctionComponent getOctionComponent(Context context);
}
