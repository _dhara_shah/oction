package com.octiontest.android.di;


import android.content.Context;
import android.support.annotation.NonNull;

import com.octiontest.android.dagger2.OctionComponent;

public class OctionInjector {
    public static OctionComponent from(@NonNull final Context context) {
        final OctionComponentProvider provider = (OctionComponentProvider) context.getApplicationContext();
        return provider.getOctionComponent(context);
    }
}
