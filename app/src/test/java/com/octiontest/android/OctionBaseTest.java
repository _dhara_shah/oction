package com.octiontest.android;

import android.content.Context;
import android.support.annotation.CallSuper;

import com.octiontest.android.dagger2.DaggerOctionComponent;
import com.octiontest.android.dagger2.OctionComponent;
import com.octiontest.android.dagger2.OctionModule;
import com.octiontest.android.di.OctionInjector;

import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;

import static org.mockito.Matchers.any;

@PrepareForTest({OctionInjector.class})
public class OctionBaseTest extends CoreBaseTest {

    @Mock
    protected OctionModule octionModule;

    @Override
    @CallSuper
    public void setUp() throws Exception {
        super.setUp();

        final OctionComponent octionComponent = DaggerOctionComponent
                .builder()
                .coreComponent(coreComponent)
                .octionModule(octionModule)
                .build();
        PowerMockito.mockStatic(OctionInjector.class);
        PowerMockito.when(OctionInjector.from(any(Context.class))).thenReturn(octionComponent);
    }
}