package com.octiontest.android.home.model;

import com.octiontest.android.OctionBaseTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.eq;

@RunWith(PowerMockRunner.class)
public class MainModelAdapterImplTest extends OctionBaseTest {
    private static final int POSITION = 0;
    private final String[] pageTitleArray = new String[]{TITLE};
    private static final String TITLE = "title";
    private MainModelAdapterImpl mainModelAdapter;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        mainModelAdapter = new MainModelAdapterImpl(pageTitleArray);
    }

    @Test
    public void testGetCount() {
        assertTrue(mainModelAdapter.getCount() == 1);
    }

    @Test
    public void testGetTitle() {
        assertEquals(mainModelAdapter.getTitle(eq(POSITION)), TITLE);
    }
}
