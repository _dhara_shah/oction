package com.octiontest.android.home.router;

import android.support.v4.app.FragmentActivity;

import com.octiontest.android.OctionBaseTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Mockito.verify;

@RunWith(PowerMockRunner.class)
public class MainRouterImplTest extends OctionBaseTest {
    private MainRouterImpl mainRouter;

    @Mock
    FragmentActivity activity;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        mainRouter = new MainRouterImpl(activity);
    }

    @Test
    public void testOnClose() {
        mainRouter.close();
        verify(activity).onBackPressed();
    }
}
