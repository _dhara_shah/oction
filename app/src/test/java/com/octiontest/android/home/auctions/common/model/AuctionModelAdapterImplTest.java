package com.octiontest.android.home.auctions.common.model;

import com.octiontest.android.OctionBaseTest;
import com.octiontest.android.core.network.entity.auctions.Auction;
import com.octiontest.android.core.network.entity.auctions.AuctionMedia;
import com.octiontest.android.core.network.entity.auctions.AuctionResponse;
import com.octiontest.android.core.utils.StringUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.internal.verification.VerifyNoMoreInteractions.verifyNoMoreInteractions;

@RunWith(PowerMockRunner.class)
@PrepareForTest({AuctionType.class, StringUtils.class})
public class AuctionModelAdapterImplTest extends OctionBaseTest {
    private static final int SIZE = 1;
    private static final int POS = 0;
    private static final String TITLE = "TITLE";
    private static final String DESC = "DESC";
    private static final String PRODUCT_PRICE = "1000000";
    private static final String MIN_PRICE = "4";
    private static final String PRICE = "0.01";
    private static final String PRODUCT_CURR = "HKD";
    private static final String MEDIA_URL = "MEDIA_URL";
    private static final AuctionType typeCurrent = AuctionType.AUCTION_CURRENT;
    private AuctionModelAdapterImpl modelAdapter;
    private List<AuctionResponse> auctionList;

    @Mock
    private Auction auction;
    @Mock
    private AuctionResponse auctionResponse;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        PowerMockito.mockStatic(AuctionType.class, StringUtils.class);

        auctionList = new ArrayList<>();
        auctionList.add(auctionResponse);

        modelAdapter = AuctionModelAdapterImpl.create(auctionList, typeCurrent);
        modelAdapter.type = typeCurrent;
    }

    @Test
    public void testGetCount() {
        assertEquals(modelAdapter.getCount(), SIZE);
    }

    @Test
    public void testGetTitle() {
        when(auctionResponse.getAuction()).thenReturn(auction);
        when(auction.getTitle()).thenReturn(TITLE);
        modelAdapter.getTitle(POS);
        verify(auctionResponse).getAuction();
        verify(auction).getTitle();
    }

    @Test
    public void testGetDescription() {
        when(auctionResponse.getAuction()).thenReturn(auction);
        when(auction.getDescription()).thenReturn(DESC);
        modelAdapter.getDescription(POS);
        verify(auctionResponse).getAuction();
        verify(auction).getDescription();
    }

    @Test
    public void testGetRetailPrice() {
        when(auctionResponse.getAuction()).thenReturn(auction);
        when(auction.getProductPrice()).thenReturn(PRODUCT_PRICE);
        when(auction.getProductCurrency()).thenReturn(PRODUCT_CURR);
        when(StringUtils.getNonNullNumberValue(PRODUCT_PRICE)).thenReturn(PRODUCT_PRICE);
        modelAdapter.getRetailPrice(POS);

        verify(auction).getProductPrice();
        verify(auction).getProductCurrency();
        verify(auctionResponse, atLeast(2)).getAuction();
    }

    @Test
    public void testGetMinimumPrice() {
        when(auctionResponse.getAuction()).thenReturn(auction);
        when(auction.getMinimumPrice()).thenReturn(MIN_PRICE);
        when(StringUtils.getNonNullNumberValue(MIN_PRICE)).thenReturn(MIN_PRICE);
        modelAdapter.getMinimumPrice(POS);
        verify(auctionResponse).getAuction();
        verify(auction).getMinimumPrice();
    }

    @Test
    public void testGetImage() {
        final List<AuctionMedia> mediaList = new ArrayList<>();
        final AuctionMedia media = mock(AuctionMedia.class);
        mediaList.add(media);

        when(auctionResponse.getMediaList()).thenReturn(mediaList);
        when(media.getMedia()).thenReturn(MEDIA_URL);

        modelAdapter.getImage(POS);
        verify(auctionResponse).getMediaList();
        verify(media).getMedia();
        verifyNoMoreInteractions(media);
    }

    @Test
    public void testGetCurrency() {
        when(auctionResponse.getAuction()).thenReturn(auction);
        when(auction.getCurrency()).thenReturn(PRODUCT_CURR);
        modelAdapter.getCurrency(POS);
        verify(auctionResponse).getAuction();
        verify(auction).getCurrency();
    }

    @Test
    public void testGetBidPrice() {
        when(auctionResponse.getAuction()).thenReturn(auction);
        when(auction.getPrice()).thenReturn(PRICE);
        when(StringUtils.getNonNullNumberValue(PRICE)).thenReturn(PRICE);
        modelAdapter.getBidPrice(POS);
        verify(auctionResponse).getAuction();
        verify(auction).getPrice();
    }

    @Test
    public void testCalculateSeconds() {
        assertEquals(modelAdapter.calculateSeconds(10), 1.0);
    }

    @Test
    public void testIsCurrentAuction() {
        modelAdapter.isACurrentAuction();
        assertEquals(modelAdapter.type, AuctionType.AUCTION_CURRENT);
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(auctionResponse, auction);
    }
}
