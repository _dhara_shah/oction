package com.octiontest.android.home.auctions.common.model;

import com.octiontest.android.OctionBaseTest;
import com.octiontest.android.core.network.api.Listener;
import com.octiontest.android.core.network.api.ServiceError;
import com.octiontest.android.core.network.entity.auctions.AuctionResponse;
import com.octiontest.android.home.auctions.common.data_source.AuctionsDataSource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.List;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
public class AuctionInteractorImplTest extends OctionBaseTest {
    private static final String ERROR_MESSAGE = "ERROR";
    private AuctionsInteractorImpl interactor;

    @Mock
    AuctionsDataSource dataSource;
    @Mock
    AuctionsListener listener;
    @Captor
    ArgumentCaptor<Listener<List<AuctionResponse>>> argumentCaptor;
    @Mock
    List<AuctionResponse> response;
    @Mock
    ServiceError error;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        when(octionModule.provideAuctionsDataSource()).thenReturn(dataSource);
        interactor = new AuctionsInteractorImpl();
    }

    @Test
    public void testLoadAuctionsSuccess() {
        interactor.loadAuctions(listener);
        verify(dataSource).getCurrentAuctions(argumentCaptor.capture());
        argumentCaptor.getValue().onSuccess(response);
        verify(listener).onAuctionsLoadSuccess();
    }

    @Test
    public void testLoadAuctionsError() {
        when(error.getErrorMessage()).thenReturn(ERROR_MESSAGE);
        interactor.loadAuctions(listener);
        verify(dataSource).getCurrentAuctions(argumentCaptor.capture());
        argumentCaptor.getValue().onError(error);
        verify(error).getErrorMessage();
        verify(listener).onAuctionsLoadError(eq(ERROR_MESSAGE));
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(dataSource, response, error, listener);
    }
}
