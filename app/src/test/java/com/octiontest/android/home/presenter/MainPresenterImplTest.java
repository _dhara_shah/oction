package com.octiontest.android.home.presenter;

import com.octiontest.android.OctionBaseTest;
import com.octiontest.android.home.model.MainModelAdapter;
import com.octiontest.android.home.router.MainRouter;
import com.octiontest.android.home.view.MainView;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(PowerMockRunner.class)
public class MainPresenterImplTest extends OctionBaseTest {
    private MainPresenterImpl presenter;

    @Mock
    MainView view;
    @Mock
    MainRouter router;
    @Mock
    MainModelAdapter modelAdapter;
    @Captor
    ArgumentCaptor<MainModelAdapter> argumentCaptor;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        presenter = new MainPresenterImpl(view, router);
    }

    @Test
    public void testHandleOnViewCreated() {
        presenter.handleOnViewCreated();
        verify(view).initViews(argumentCaptor.capture());
    }

    @Test
    public void testHandleOnBackPressed() {
        presenter.handleOnBackPressed();
        verify(router).close();
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(view, router);
    }
}
