package com.octiontest.android.home.auctions.upcoming.presenter;

import com.octiontest.android.OctionBaseTest;
import com.octiontest.android.home.auctions.common.model.AuctionModelAdapter;
import com.octiontest.android.home.auctions.common.model.AuctionsInteractor;
import com.octiontest.android.home.auctions.common.model.AuctionsListener;
import com.octiontest.android.home.auctions.common.view.CommonView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
public class UpcomingAuctionsPresenterImplTest extends OctionBaseTest {
    private UpcomingAuctionsPresenterImpl presenter;
    @Mock
    AuctionsInteractor interactor;
    @Mock
    CommonView view;
    @Captor
    ArgumentCaptor<AuctionsListener> argumentCaptor;
    @Captor
    ArgumentCaptor<AuctionModelAdapter> modelAdapterArgumentCaptor;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        when(octionModule.provideAuctionsInteractor()).thenReturn(interactor);
        presenter = UpcomingAuctionsPresenterImpl.from(view);
    }

    @Test
    public void testHandleOnViewCreated() {
        presenter.handleOnViewCreated();
        verify(view).initViews();
        verify(view).showProgress();
        verify(interactor).loadUpcomingAuctions(argumentCaptor.capture());
    }

    @Test
    public void testOnAuctionsLoadSuccess() {
        presenter.onAuctionsLoadSuccess();
        verify(view).hideProgress();
        verify(view).updateData(modelAdapterArgumentCaptor.capture());
    }
}
